
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class Assignment5 extends JFrame implements ActionListener{

	//here we are creating the text fields and button to be implemented in the JPanel
	private JButton btnClick;
	private JTextField score1 = new JTextField("Enter Assignment 1 Score");
	private JTextField score2 = new JTextField("Enter Assignment 2 Score");
	private JTextField score3 = new JTextField("Enter Assignment 3 Score");
	private JTextField score4 = new JTextField("Enter Assignment 4 Score");
	
	private JTextField weight1 = new JTextField("Enter Assignment 1 Weight");
	private JTextField weight2 = new JTextField("Enter Assignment 2 Weight");
	private JTextField weight3 = new JTextField("Enter Assignment 3 Weight");
	private JTextField weight4 = new JTextField("Enter Assignment 4 Weight");
	
	private JPanel panel1;
	
	public Assignment5() {
		//this is where the panel is defined and we add the text fields and button to it
		panel1 = new JPanel();
		panel1.setBackground(Color.DARK_GRAY);
	
		panel1.add(score1);
		panel1.add(weight1);
		panel1.add(score2);
		panel1.add(weight2);
		panel1.add(score3);
		panel1.add(weight3);
		panel1.add(score4);
		panel1.add(weight4);
		
		//here I define the specifics of the button 
		btnClick = new JButton("Calculate");
		add(panel1, BorderLayout.CENTER);
		add(btnClick,BorderLayout.SOUTH);
		btnClick.addActionListener(this);
		
		//here I define the specifics of the GUI Window 
		setSize(350,400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
	}
	
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "Please enter test scores percentages as whole numbers, and their weight as a decimal");
		new Assignment5();
	}
	
	public void actionPerformed(ActionEvent e) {
		// here I convert the text field values into double values for score and weight 
		double scoreint1 = Double.parseDouble(score1.getText());
		double scoreint2 = Double.parseDouble(score2.getText());
		double scoreint3 = Double.parseDouble(score3.getText());
		double scoreint4 = Double.parseDouble(score4.getText());
		
		double weightint1 = Double.parseDouble(weight1.getText());
		double weightint2 = Double.parseDouble(weight2.getText());
		double weightint3 = Double.parseDouble(weight3.getText());
		double weightint4 = Double.parseDouble(weight4.getText());
		
		//this is the calculations used to find the weighted average
		double test1calc = scoreint1 * weightint1;
		double test2calc = scoreint2 * weightint2;
		double test3calc = scoreint3 * weightint3;
		double test4calc = scoreint4 * weightint4;
		
		double finalscore = test1calc + test2calc + test3calc + test4calc;
		// the weighted average is displayed here
		JOptionPane.showMessageDialog(null,"The Weighted Average of the Scores is " + finalscore);
	}

}
